import React, { Component, PropTypes } from "react";
import { View } from "react-native";

export default class GenericScreen extends Component {
  static childContextTypes = {
    navigate: PropTypes.func
  };

  getChildContext() {
    return {
      navigate: this.props.navigation.navigate
    };
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: "row" }}>
        {React.cloneElement(this.props.screen, { ...this.props })}
      </View>
    );
  }
}
