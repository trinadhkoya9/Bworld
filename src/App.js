import React from "react";
import { StackNavigator } from "react-navigation";
import GenericScreen from "./screens/GenericScreen";
import LoginScreen from "./screens/LoginScreen";

/**
 * Renders the router of the application and passes its props to its children
 * through context
 */
const App = StackNavigator(
  {
    login: {
      screen: props => <GenericScreen {...props} screen={<LoginScreen />} />
    }
  },
  { navigationOptions: { header: null } }
);

export default App;
